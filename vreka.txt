<?php

require_once 'total.php';

class Vreka implements Total
{
	public $cena_vreka;
	public $broj_vreka;

	public function __construct($cena_vreka, $broj_vreka)
	{
		$this->cena_vreka = $cena_vreka;
		$this->broj_vreka = $broj_vreka;
	}

	public function total()
	{
		$cena_vr = $this->cena_vreka * $this->broj_vreka;
		return $cena_vr;
	}
}
